import 'jquery/dist/jquery.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'sweetalert/dist/sweetalert.min.js';
import 'sweetalert/dist/sweetalert.css';
import eventAddForm from './components/eventAddForm';
import eventInfo from './components/eventInfo';
import event from './components/event';
import calendarDay from './components/calendarDay';
import calendar from './components/calendar';

angular.module('app', [calendar, calendarDay, event, eventAddForm, eventInfo]);
