import ng from 'angular';

import EventAddFormService from './event-add-form.service';
import EventAddFormComponent from './event-add-form.component';

export default ng.module('app.components.eventAddForm', [])
  .service('EventAddFormService', EventAddFormService)
  .component('eventAddForm', EventAddFormComponent)
  .name;
