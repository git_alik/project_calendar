import template from './event-add-form.html';
import controller from './event-add-form.controller';
import style from './style.less';

export default {
  template,
  controller,
  style,
  bindings: {
    checkedDay: '<',
    updateListEvents: '&'
  }
};
