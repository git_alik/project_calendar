export default class EventAddFormService {
  constructor() {
    'ngInject';
    this.eventEdit = {
      _id: undefined,
      title: '',
      description: '',
      startHH: 0,
      startMM: 0,
      endHH: 0,
      endMM: 0
    };
    this.checkedDay = {
      day: Date.now()
    };
  }

  setDay(day) {
    this.checkedDay.day = day;
    this.eventEdit._id = undefined;
    this.eventEdit.title = '';
    this.eventEdit.description = '';
    this.eventEdit.startHH = 0;
    this.eventEdit.startMM = 0;
    this.eventEdit.endHH = 1;
    this.eventEdit.endMM = 0;

  }

  editEvent(event) {
    this.eventEdit._id = event._id;
    this.eventEdit.title = event.title;
    this.eventEdit.description = event.description;
    this.eventEdit.startHH = new Date(event.start_time).getHours();
    this.eventEdit.startMM = new Date(event.start_time).getMinutes();
    this.eventEdit.endHH = new Date(event.end_time).getHours();
    this.eventEdit.endMM = new Date(event.end_time).getMinutes();

    this.checkedDay.day = event.start_time;
  }
}
