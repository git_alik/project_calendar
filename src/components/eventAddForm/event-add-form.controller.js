export default class EventAddFormController {
  constructor(CalendarService, EventAddFormService) {
    'ngInject';
    this.CalendarService = CalendarService;
    this.EventAddFormService = EventAddFormService;
    this.newEvent = this.EventAddFormService.eventEdit;
    this.checkedDay = this.EventAddFormService.checkedDay;
  }

  save(form) {
    if (form.$invalid) {
      return;
    }

    if (this.newEvent.startHH > this.newEvent.endHH) {
      swal('Oops...', 'Invalid time ends', 'error');
      return;
    }
    if (this.newEvent.startHH === this.newEvent.endHH && this.newEvent.startMM > this.newEvent.endMM) {
      swal('Oops...', 'Invalid time ends', 'error');
      return;
    }

    const checkedDay = new Date(this.checkedDay.day);
    const start = checkedDay.setHours(+this.newEvent.startHH, +this.newEvent.startMM, 0, 0);
    const end = checkedDay.setHours(+this.newEvent.endHH, +this.newEvent.endMM, 0, 0);

    const event = {
      title: this.newEvent.title,
      description: this.newEvent.description,
      start,
      end
    };

    if (this.newEvent._id === undefined) {
      this.CalendarService.addEvent(event).then((result) => {
        this.updateListEvents({
          day: this.checkedDay.day
        });
      });
    } else {
      event._id = this.newEvent._id;
      this.CalendarService.updateEvent(event).then((result) => {
        this.updateListEvents({
          day: new Date(this.checkedDay.day)
        });
      });
    }

    $('#myModal').modal('hide');
    this.newEvent.title = '';
    this.newEvent.description = '';
  }
}
