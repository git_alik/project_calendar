import template from './event-info.html';
import controller from './event-info.controller';
import style from './style.less';

export default {
  template,
  controller,
  style,
  bindings: {
    event: '<',
    removeEvent: '&'
  }
};
