import ng from 'angular';

import EventInfoComponent from './event-info.component';

export default ng.module('app.components.eventInfo', [])
  .component('eventInfo', EventInfoComponent)
  .name;
