export default class EventInfoController {
  constructor(EventAddFormService) {
    'ngInject';
    this.EventAddFormService = EventAddFormService;
  }

  editEvent() {
    this.EventAddFormService.editEvent(this.event);
  }
}
