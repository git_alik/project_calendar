export default class CalendarDayController {
  constructor(CalendarService, EventAddFormService) {
    'ngInject';
    this.CalendarService = CalendarService;
    this.EventAddFormService = EventAddFormService;
  }

  showCheckedDay() {
    this.showDay({
      checkedDay: this.dayEvents.dateOfDay
    });
  }

  removeEvent(oldEvent) {
    this.CalendarService.removeEvent(oldEvent._id)
      .then(() => this.CalendarService.listEvents(this.dayEvents.dateOfDay, false)
        .then((result) => {
          this.dayEvents.events = result;
        })
      );
  }

  sendDay() {
    this.EventAddFormService.setDay(this.dayEvents.dateOfDay);
  }
}
