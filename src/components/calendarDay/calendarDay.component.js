import template from './calendarDay.html';
import controller from './calendarDay.controller';
import style from './style.less';

export default {
  template,
  controller,
  style,
  bindings: {
    displayWeek: '=',
    dayEvents: '<',
    showDay: '&',
    checkedDay: '='
  }
};
