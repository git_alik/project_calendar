import ng from 'angular';

import CalendarDayComponent from './calendarDay.component';

export default ng.module('app.components.calendarDay', [])
  .component('calendarDay', CalendarDayComponent)
  .name;
