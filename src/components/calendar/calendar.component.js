import template from './calendar.html';
import controller from './calendar.controller';
import style from './style.less';

export default {
  template,
  controller,
  style
};
