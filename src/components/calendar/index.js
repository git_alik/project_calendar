import ng from 'angular';

import CalendarComponent from './calendar.component';
import CalendarService from './calendar.service';

export default ng.module('app.components.calendar', [])
  .component('calendar', CalendarComponent)
  .service('CalendarService', CalendarService)
  .name;
