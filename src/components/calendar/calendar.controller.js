export default class CalendarController {
  constructor(CalendarService) {
    'ngInject';
    this.CalendarService = CalendarService;
  }

  $onInit() {
    this.checkedDay = new Date();
    this.showWeekEvents(this.checkedDay);
  }

  showPreviousEvents() {
    const checkedDay = this.checkedDay;
    if (this.displayWeek === false) {
      checkedDay.setDate(this.checkedDay.getDate() - 1);
      this.showDayEvents(checkedDay);
    } else {
      checkedDay.setDate(this.showingEvents[0].dateOfDay.getDate() - 7);
      this.showWeekEvents(checkedDay);
    }
  }

  showNextEvents() {
    const checkedDay = this.checkedDay;
    if (this.displayWeek === false) {
      checkedDay.setDate(this.checkedDay.getDate() + 1);
      this.showDayEvents(checkedDay);
    } else {
      checkedDay.setDate(this.showingEvents[0].dateOfDay.getDate() + 7);
      this.showWeekEvents(checkedDay);
    }
  }

  showToday() {
    this.checkedDay = new Date();
    this.showDayEvents(this.checkedDay);
  }

  showDay(checkedDay) {
    this.checkedDay = new Date(checkedDay);
    this.showDayEvents(this.checkedDay);
  }

  showDayEvents(day) {
    const checkedDay = day;
    const isWeek = false;
    this.displayWeek = isWeek;

    this.showingEvents = [{
      dateOfDay: checkedDay,
      events: []
    }];

    this.CalendarService.listEvents(checkedDay, isWeek).then((events) => {
      this.events = events;
    }).then(() => {
      this.events.forEach((event) => {
        this.showingEvents[0].events.push(event);
      });
      const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
      this.titleCalendar = `${months[checkedDay.getMonth()]} ${checkedDay.getDate()}, ${checkedDay.getFullYear()}`;
    });
  }

  showCurrentWeek() {
    this.showWeekEvents(new Date());
  }

  showWeekCheckedDay() {
    this.showWeekEvents(this.checkedDay || new Date());
  }

  showWeekEvents(day) {
    const checkedDay = new Date(day);
    const isWeek = true;
    this.displayWeek = isWeek;

    let numberDay = checkedDay.getDay();
    if (numberDay === 0) {
      numberDay = 6;
    } else {
      numberDay -= 1;
    }

    const firstDayOfWeek = new Date(checkedDay.getTime() - (numberDay * 86400000));
    firstDayOfWeek.setHours(0, 0, 0, 0);

    this.showingEvents = [];
    for (let i = 0; i < 7; i += 1) {
      this.showingEvents.push({
        dateOfDay: new Date(firstDayOfWeek),
        events: []
      });
      firstDayOfWeek.setHours(24);
    }

    this.CalendarService.listEvents(checkedDay, isWeek).then((events) => {
      this.events = events;
    }).then(() => {
      this.events.forEach((event) => {
        let dayOfEvent = new Date(event.start_time);
        dayOfEvent = dayOfEvent.getDay();
        if (dayOfEvent === 0) {
          dayOfEvent = 6;
        } else {
          dayOfEvent -= 1;
        }
        this.showingEvents[dayOfEvent].events.push(event);
      });
      const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
      this.titleCalendar = `${months[checkedDay.getMonth()]} ${checkedDay.getFullYear()}`;
    });
  }

  updateListEvents(day) {
    const isWeek = false;
    this.CalendarService.listEvents(day, isWeek).then((result) => {
      let dayOfEvent = day.getDay();
      if (dayOfEvent === 0) {
        dayOfEvent = 6;
      } else {
        dayOfEvent -= 1;
      }

      if (this.displayWeek === false) {
        dayOfEvent = 0;
      }
      this.showingEvents[dayOfEvent].events = result;
    });
  }
}
