export default class CalendarService {
  constructor($http) {
    'ngInject';
    this.$http = $http;
  }

  listEvents(day, isWeek) {
    const checkedDay = day;
    checkedDay.setHours(0, 0, 0, 0);

    let startInterval;
    let endInterval;
    if (isWeek) {
      let numberDay = checkedDay.getDay();

      if (numberDay === 0) {
        numberDay = 6;
      } else {
        numberDay -= 1;
      }

      startInterval = checkedDay.getTime() - (numberDay * 86400000);
      endInterval = checkedDay.getTime() + ((7 - numberDay) * 86400000);
    } else {
      startInterval = checkedDay.getTime();
      endInterval = checkedDay.getTime() + 86400000;
    }

    return this.$http.get(`${API_URL}/events`, {
      params: {
        from: startInterval,
        to: endInterval
      }
    }).then(events => events.data);
  }

  addEvent(newEvent) {
    return this.$http.post(`${API_URL}/events`, newEvent);
  }

  updateEvent(event) {
    return this.$http.put(`${API_URL}/events`, event);
  }

  removeEvent(id) {
    return this.$http.delete(`${API_URL}/events/${id}`);
  }
}
