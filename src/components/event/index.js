import ng from 'angular';

import EventComponent from './event.component';

export default ng.module('app.components.event', [])
  .component('event', EventComponent)
  .name;
