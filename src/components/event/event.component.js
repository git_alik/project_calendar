import template from './event.html';
import controller from './event.controller';
import style from './style.less';

export default {
  template,
  controller,
  style,
  bindings: {
    event: '<',
    removeEvent: '&'
  }
};
