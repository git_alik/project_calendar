export default class EventController {
  constructor(CalendarService) {
    'ngInject';
    this.CalendarService = CalendarService;
    this.infoIsShow = false;
  }

  deleteEvent() {
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this imaginary file!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, delete it!',
      closeOnConfirm: true
    }, () => {
      this.removeEvent({
        event: this.event
      });
    });
  }
}
